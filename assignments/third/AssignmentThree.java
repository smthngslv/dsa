package dsa.assignments.third;

import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.util.*;
import javafx.util.Pair;

// Ivan Izmailov.
// B19-01. iv.izmailov@innopolis.university

public class AssignmentThree {
    /**
     * Reads the graph.
     * Complexity: O(V + E). V - number of vertices; E - number of edges.
     * @param isWeighted If false - each edge has null as weight.
     * @return Returns a read graph.
     */
    private static Graph<Integer, Integer> readGraph(boolean isWeighted) {
        FastReader scanner = new FastReader(System.in);

        int numberOfVertices = scanner.nextInt();
        int numberOfEdges = scanner.nextInt();

        Graph<Integer, Integer> graph = new Graph<>();

        for (int i = 1; i < numberOfVertices + 1; ++i) {
            graph.insertVertex(i);
        }

        for (int i = 0; i < numberOfEdges; ++i) {
            graph.insertEdge(scanner.nextInt(), scanner.nextInt(), isWeighted ? scanner.nextInt(): null);
        }

        return graph;
    }

    private static void ConnectedGraphTask() {
        Graph<Integer, Integer> graph = readGraph(false);

        Pair<HashSet<Integer>, HashSet<Integer>> info = graph.analyzeConnectivity();

        if (info.getKey().isEmpty()) {
            System.out.println("GRAPH IS CONNECTED"); return;
        }

        System.out.printf("VERTICES %d AND %d ARE NOT CONNECTED BY A PATH",
                info.getKey().iterator().next(),
                info.getValue().iterator().next());
    }

    private static void ComponentsDictionaryTask() {
        Graph<Integer, Integer> graph = readGraph(false);

        HashMap<Integer, Integer> componentsMap = graph.vertexComponents();

        for (int i = 1; i < graph.getVertices().size() + 1; ++i) {
            System.out.print(componentsMap.get(i) + " ");
        }
    }

    private static void MinimumSpanningForestTask() {
        Graph<Integer, Integer> graph = readGraph(true);

        ArrayList<Graph<Integer, Integer>> msf = graph.minimumSpanningForest().split();

        System.out.println(msf.size());

        for (Graph<Integer, Integer> g : msf) {
            System.out.println(g.getVertices().size() + " " + g.getVertices().iterator().next());

            for (Graph.Edge<Integer, Integer> edge : g.getEdges()) {
                System.out.println(edge.getFrom() + " " + edge.getTo() + " " + edge.getWeight());
            }
        }
    }

    public static void main(String[] args) {
        //ConnectedGraphTask();
        //ComponentsDictionaryTask();
        MinimumSpanningForestTask();
    }
}

/**
 * Represents a system of disjoint sets.
 * I use path compression and union by size.
 * This allows to get amortized time of O(a(N)), where a(N) is
 * inverse Ackermann function, which practically equals to 5,
 * because the Ackermann function grows very fast.
 * @param <T> Type of items in sets.
 */
class DisjointSystemSets<T> {
    // Store sizes of sets. Size equals to number of items.
    private int[] size;
    private int[] parent;

    // This map I use to make the class generic.
    private HashMap<T, Integer> map;

    /**
     * Create a new system of disjoint sets.
     * Complexity: O(N).
     * @param items Items to store in sets.
     */
    public DisjointSystemSets(Iterable<T> items) {
        map = new HashMap<>();

        // Store a number of items.
        int index = 0;

        // Fill the map. Each element gets unique index.
        for (T item : items) {
            map.put(item, index++);
        }

        size = new int[index];
        parent = new int[index];

        // Initialize.
        for (int i = 0; i < index; ++i) {
            // Only one item per set so far.
            size[i] = 1;
            parent[i] = i;
        }
    }


    /**
     * To get the index of the set, in which the value is stored.
     * Complexity: Amortized is O(a(N)).
     * @param value A item of one of sets.
     * @return Returns a index of the set.
     */
    public int getSetIndex(T value) {
        return find(map.get(value));
    }

    /**
     * Union two sets.
     * Complexity: Amortized is O(a(N)).
     * @param left The item from the first set.
     * @param right Thi item from the second set.
     */
    public void union(T left, T right) {
        int leftRoot = getSetIndex(left);
        int rightRoot = getSetIndex(right);

        // Already joined.
        if (leftRoot == rightRoot) {
            return;
        }

        // Join the node with smaller size to the bigger one.
        if (size[leftRoot] < size[rightRoot]) {
            parent[leftRoot] = rightRoot;
            size[rightRoot] += size[leftRoot];

            return;
        }

        parent[rightRoot] = leftRoot;
        size[leftRoot] += size[rightRoot];
    }

    /**
     * Find a root of the tree, in which the index is located.
     * Complexity: Amortized is O(a(N)).
     * @param index Index of the item.
     * @return Returns a root of the index.
     */
    private int find(int index) {
        if (parent[index] == index) {
            return index;
        }

        // Path compression. I redirect each node in my way to the root.
        // And next time, I can find it faster.
        parent[index] = find(parent[index]);

        return parent[index];
    }
}

/**
 * Represents a undirected weighted graph using adjacency list.
 * @param <V> Type of the data stored in nodes.
 * @param <E> Type of the weight.
 */
class Graph<V, E extends Comparable<E>> {
    /**
     * Represents an edge.
     * @param <V> Type of the data stored in nodes.
     * @param <E> Type of the weight.
     */
    public static class Edge<V, E extends Comparable<E>> {
        private V from;
        private V to;
        private E weight;

        /**
         * Create a new edge.
         * Complexity: O(1).
         * @param from Vertex from.
         * @param to Target vertex.
         * @param weight a weight. Can be null.
         */
        public Edge(V from, V to, E weight) {
            this.from = from;
            this.to = to;
            this.weight = weight;
        }


        /**
         * Complexity: O(1).
         * @return Returns the `from` vertex.
         */
        public V getFrom() {
            return from;
        }

        /**
         * Complexity: O(1).
         * @return Returns the `to` vertex.
         */
        public V getTo() {
            return to;
        }

        /**
         * Complexity: O(1).
         * @return Returns the weight.
         */
        public E getWeight() {
            return weight;
        }

        /**
         * Get a vertex of the opposite side of the edge.
         * Complexity: O(1).
         * @param vertex Vertex.
         * @return Returns the opposite vertex.
         * @throws IllegalArgumentException Throws an exception,
         * when the vertex doesn't belong to the edge.
         */
        public V opposite(V vertex) throws IllegalArgumentException{
            if (from.equals(vertex)) {
                return to;
            }
            else if (to.equals(vertex)) {
                return from;
            }

            throw new IllegalArgumentException("The vertex doesn't belong to this edge.");
        }
    }

    HashMap<V, HashSet<Edge<V, E>>> adjacencyList;

    /**
     * Creates a new empty graph.
     * Complexity: O(1).
     */
    public Graph() {
        adjacencyList = new HashMap<>();
    }

    /**
     * Creates a new graph with `vertices` and `edges`.
     * Complexity: O(V + E). V - number of vertices; E - number of edges.
     * @param vertices A collection of vertices.
     * @param edges A collection of edges.
     */
    public Graph(Iterable<V> vertices, Iterable<Edge<V, E>> edges) {
        this();

        insertVertices(vertices);
        insertEdges(edges);
    }


    /**
     * Complexity: O(1).
     * @return Returns the set of vertices.
     */
    public Set<V> getVertices() {
        return adjacencyList.keySet();
    }

    /**
     * Complexity: O(V + E). V - number of vertices; E - number of edges.
     * @return Returns the set of edges.
     */
    public Set<Edge<V, E>> getEdges() {
        HashSet<Edge<V, E>> edges = new HashSet<>();

        for (V vertex : adjacencyList.keySet()) {
            edges.addAll(adjacencyList.get(vertex));
        }

        return edges;
    }

    /**
     * Inserts a vertex into the graph.
     * Complexity: O(1).
     * @param vertex A vertex to be inserted.
     * @throws IllegalArgumentException Throws an exception
     * if the vertex is already in the graph.
     */
    public void insertVertex(V vertex) throws IllegalArgumentException {
        if (adjacencyList.containsKey(vertex)) {
            throw new IllegalArgumentException("Vertex already exists.");
        }

        adjacencyList.put(vertex, new HashSet<>());
    }

    /**
     * Inserts a collection of vertices into the graph.
     * Complexity: O(V). V - number of vertices.
     * @param vertices Iterable collection of vertices.
     */
    public void insertVertices(Iterable<V> vertices) {
        for (V vertex : vertices) {
            insertVertex(vertex);
        }
    }

    /**
     * Inserts a new edge into the graph.
     * Complexity: O(1).
     * @param edge An edge to be inserted.
     * @throws IllegalArgumentException Throws an exception
     * if the edge already exists or the graph doesn't contain vertices of the edge.
     */
    public void insertEdge(Edge<V, E> edge) throws IllegalArgumentException {
        if (!(adjacencyList.containsKey(edge.from) && adjacencyList.containsKey(edge.to))) {
            throw new IllegalArgumentException("One of the vertices doesn't exist.");
        }

        HashSet<Edge<V, E>> edgesFrom = adjacencyList.get(edge.from);
        HashSet<Edge<V, E>> edgesTo = adjacencyList.get(edge.to);

        if (edgesFrom.contains(edge) || edgesTo.contains(edge)) {
            throw new IllegalArgumentException("Edge already exists.");
        }

        edgesFrom.add(edge);
        edgesTo.add(edge);
    }

    /**
     * Inserts a new edge into the graph.
     * Complexity: O(1).
     * @param from Vertex from.
     * @param to Target vertex.
     * @param weight a weight. Can be null.
     */
    public void insertEdge(V from, V to, E weight) {
        insertEdge(new Edge<>(from, to, weight));
    }

    /**
     * Inserts a collection of edges into the graph.
     * Complexity: O(E). E - number of edges.
     * @param edges Iterable collection of edges.
     */
    public void insertEdges(Iterable<Edge<V, E>> edges) {
        for (Edge<V, E> edge : edges) {
            insertEdge(edge);
        }
    }

    /**
     * Split a graph (Forest) into subgraphs (Trees).
     * Complexity: O(V + E). V - number of vertices; E - number of edges.
     * @return An array list of trees.
     */
    public ArrayList<Graph<V, E>> split() {
        ArrayList<Graph<V, E>> graphs = new ArrayList<>();
        HashSet<V> allVertices = new HashSet<>(getVertices());

        while (!allVertices.isEmpty()) {
            // Start dfs from random vertex.
            Pair<HashSet<V>, HashSet<Edge<V, E>>> dfsResult = dfs(allVertices.iterator().next());

            // Build a new graph with edges and vertices, which are visited by dfs.
            graphs.add(new Graph<>(dfsResult.getKey(), dfsResult.getValue()));

            // Remove vertices from all vertices.
            allVertices.removeAll(dfsResult.getKey());
        }

        return graphs;
    }

    /**
     * Analyze a connectivity of the graph.
     * Complexity: O(V + E). V - number of vertices; E - number of edges.
     * @return Returns a pair of unvisited and visited vertices.
     */
    public Pair<HashSet<V>, HashSet<V>> analyzeConnectivity() {
        // O(1).
        HashSet<V> allVertices = new HashSet<>(adjacencyList.keySet());

        // O(V + E). Start dfs from random point.
        Pair<HashSet<V>, HashSet<Edge<V, E>>> dfsInfo = dfs(allVertices.iterator().next());

        // O(V). Remove visited vertices from all vertices. So we get a unvisited vertices.
        allVertices.removeAll(dfsInfo.getKey());

        return new Pair<>(allVertices, dfsInfo.getKey());
    }

    /**
     * Complexity: O(V + E). V - number of vertices; E - number of edges.
     * @return Returns a map Vertex -> Component.
     */
    public HashMap<V, Integer> vertexComponents() {
        // O(V + E). Split graph in subgraphs.
        ArrayList<Graph<V, E>> graphs = split();

        HashMap<V, Integer> componentsMap = new HashMap<>();

        // Go through all subgraphs. Total O(V).
        for (int i = 0; i < graphs.size(); ++i) {
            // O(V) where V - vertices of subgraph. All vertices in each subgraph.
            for (V vertex : graphs.get(i).getVertices()) {
                // O(1). Fill the map.
                componentsMap.put(vertex, i + 1);
            }
        }

        return componentsMap;
    }

    /**
     * Complexity: O(V + E * logE). V - number of vertices; E - number of edges.
     * @return Returns a minimum spanning forest for the graph.
     */
    public Graph<V, E> minimumSpanningForest() {
        // O(E * logE).
        ArrayList<Edge<V, E>> sortedEdge = new ArrayList<>(getEdges());
        sortedEdge.sort(Comparator.comparing(Edge::getWeight));

        // O(V). Make a new graph with the same vertices.
        Graph<V, E> msf = new Graph<>(getVertices(), new ArrayList<>());

        // O(V).
        DisjointSystemSets<V> dss = new DisjointSystemSets<>(getVertices());

        // O(E).
        for (Edge<V, E> edge : sortedEdge) {
            // O(a(V)) ~ O(1). If two trees unlink.
            if(dss.getSetIndex(edge.from) != dss.getSetIndex(edge.to)) {
                // O(1). Link it.
                msf.insertEdge(edge);
                // O(a(V)) ~ O(1).
                dss.union(edge.from, edge.to);
            }
        }

        return msf;
    }


    /**
     * A travel via the graph.
     * Complexity: O(V + E). V - number of vertices; E - number of edges.
     * @param start A start vertex.
     * @return A pair of visited vertices and visited edges.
     */
    private Pair<HashSet<V>, HashSet<Edge<V, E>>> dfs(V start) {
        HashSet<V> visitedVertices  = new HashSet<>();
        HashSet<Edge<V, E>> visitedEdges = new HashSet<>();

        Stack<V> processing = new Stack<>();

        visitedVertices.add(start);
        processing.push(start);

        while (!processing.isEmpty()) {
            V current = processing.pop();

            for (Edge<V, E> edge : adjacencyList.get(current)) {
                V vertex = edge.opposite(current);

                if (!visitedVertices.contains(vertex)) {
                    visitedVertices.add(vertex);
                    visitedEdges.add(edge);

                    processing.push(vertex);
                }
            }
        }

        return new Pair<>(visitedVertices, visitedEdges);
    }
}

// Java is so slow in read...
// https://www.geeksforgeeks.org/fast-io-in-java-in-competitive-programming/
class FastReader {
    BufferedReader br;
    StringTokenizer st;

    public FastReader(InputStream stream)
    {
        br = new BufferedReader(new InputStreamReader(stream));
    }

    String next()
    {
        while (st == null || !st.hasMoreElements())
        {
            try
            {
                st = new StringTokenizer(br.readLine());
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    int nextInt()
    {
        return Integer.parseInt(next());
    }

    long nextLong()
    {
        return Long.parseLong(next());
    }

    double nextDouble()
    {
        return Double.parseDouble(next());
    }

    String nextLine()
    {
        String str = "";
        try
        {
            str = br.readLine();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return str;
    }
}