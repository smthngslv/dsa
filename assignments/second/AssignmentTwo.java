package dsa.assignments.second;

import javafx.util.Pair;

import java.io.DataInputStream;
import java.io.InputStream;

import java.util.Arrays;
import java.util.NoSuchElementException;

import javax.lang.model.type.NullType;


public class AssignmentTwo {
    public static void main(String[] args) {
        SweepLine.solve();
    }
}

/**
 * MEGA_FAST_WONDERFUL_BIG_JAVA_CRAZY_QUICK_SCANNER_READER.
 * Just fast read. Not mine. https://habr.com/ru/post/91283/
 */
class Scanner {
    final private int BUFFER_SIZE = 1 << 16;
    private DataInputStream din;
    private byte[] buffer;
    private int bufferPointer, bytesRead;

    public Scanner(InputStream in) {
        din = new DataInputStream(in);
        buffer = new byte[BUFFER_SIZE];
        bufferPointer = bytesRead =  0;
    }
    public String nextString(int maxSize) {
        byte[] ch = new byte[maxSize];
        int point =  0;
        try {
            byte c = read();
            while (c == ' ' || c == '\n' || c=='\r')
                c = read();
            while (c != ' ' && c != '\n' && c!='\r') {
                ch[point++] = c;
                c = read();
            }
        } catch (Exception e) {}
        return new String(ch, 0,point);
    }
    public int nextInt() {
        int ret =  0;
        boolean neg;
        try {
            byte c = read();
            while (c <= ' ')
                c = read();
            neg = c == '-';
            if (neg)
                c = read();
            do {
                ret = ret * 10 + c - '0';
                c = read();
            } while (c > ' ');

            if (neg) return -ret;
        } catch (Exception e) {}
        return ret;
    }
    public long nextLong() {
        long ret =  0;
        boolean neg;
        try {
            byte c = read();
            while (c <= ' ')
                c = read();
            neg = c == '-';
            if (neg)
                c = read();
            do {
                ret = ret * 10 + c - '0';
                c = read();
            } while (c > ' ');

            if (neg) return -ret;
        } catch (Exception e) {}
        return ret;
    }
    private void fillBuffer() {
        try {
            bytesRead = din.read(buffer, bufferPointer =  0, BUFFER_SIZE);
        } catch (Exception e) {}
        if (bytesRead == -1) buffer[ 0] = -1;
    }

    private byte read() {
        if (bufferPointer == bytesRead) fillBuffer();
        return buffer[bufferPointer++];
    }
}

/**
 * In a sweep line algorithm we use a line, which move from left to
 * right (or from down to top, in different implementations) and "scan" a 2D plane.
 * Each line segment has left (begin) and right (end) points. If we sort these points we get
 * a intervals. While sweep line is into a interval like this, the state of the system doesn't change.
 * Thus we only should check a points of start and end of the intervals. Such points are called `events`.
 * When we reach an event, it can be `new line segment appear` or `line segment ends` only.
 * In both cases we just need to check for intersect neighborhoods of the current line segment
 * which is appeared or ended. Sorting of points is O(nlog(n)). Go through all events is N and for each
 * event we ether add or remove a line segment, it is O(log(n)). Thus complexity is O(nlog(n)).
 *
 * The problem of the line segment intersect use in computer graphics, computer physics engines,
 * computational geometry. It is used for computing a directions of different transports.
 */
class SweepLine {
    /**
     * Represent a Point.
     */
    private static class Point implements Comparable<Point> {
        private int x;
        private int y;

        private LineSegment lineSegment;

        public Point(int x, int y, LineSegment lineSegment) {
            this.x = x;
            this.y = y;

            this.lineSegment = lineSegment;
        }

        /**
         * Check if this point is left (start) point of the line segment.
         * Complexity is O(1).
         * @return - true if current point is left (start) point of the corresponding line segment.
         */
        public boolean isLeft() {
            return lineSegment.left == this;
        }

        /**
         * Compare a point to the point.
         * Complexity is O(1).
         * @param other - point to compare.
         * @return - 0 if points are equal, -1 if point is less than `other` and 1 if point is bigger than `other`.
         */
        @Override
        public int compareTo(Point other) {
            if (this.x < other.x) {
                return -1;
            }
            else if (this.x > other.x) {
                return 1;
            }
            else if (this.y < other.y) {
                return -1;
            }
            else if (this.y > other.y) {
                return 1;
            }
            else if ((!this.isLeft() && !other.isLeft()) || (this.isLeft() && other.isLeft())) {
                return 0;
            }
            else if (this.isLeft()) {
                return -1;
            }

            return 1;
        }

        /**
         * Return a string which represent a point.
         * Complexity is O(1).
         * @return - a string which represent a point.
         */
        @Override
        public String toString() {
            return String.format("%d %d", x, y);
        }
    }

    /**
     * Represent a line segment.
     */
    private static class LineSegment implements Comparable<LineSegment>{
        private Point left;
        private Point right;

        private boolean isReverse;

        public LineSegment(int x1, int y1, int x2, int y2) {
            left  = new Point(x1, y1, this);
            right = new Point(x2, y2, this);

            if (left.compareTo(right) > 0) {
                isReverse = true;

                Point tmp = left; left = right; right = tmp;
            }
            else {
                isReverse = false;
            }
        }

        /**
         * Compare two line segments.
         * Complexity is O(1).
         * @param lineSegment - line segment to compare.
         * @return - 0 if line segment are equal, -1 if line segment is less than `lineSegment` and 1 if line segment is bigger than `lineSegment`.
         */
        @Override
        public int compareTo(LineSegment lineSegment) {
            if (left.compareTo(lineSegment.left) == 0) {
                return right.compareTo(lineSegment.right);
            }

            return left.compareTo(lineSegment.left);
        }

        /**
         * Return a string which represent a line segment.
         * Complexity is O(1).
         * @return - a string which represent a line segment.
         */
        @Override
        public String toString() {
            if (isReverse) {
                return String.format("%s %s", right, left);
            }

            return String.format("%s %s", left, right);
        }
    }


    /**
     * Solve the SweepLine problem.
     * Complexity is O(nlog(n)).
     */
    public static void solve() {
        Pair<LineSegment, LineSegment> intersect = findIntersect(read());

        if (intersect == null) {
            System.out.println("NO INTERSECTIONS"); return;
        }

        System.out.println("INTERSECTION");
        System.out.println(intersect.getKey());
        System.out.println(intersect.getValue());
    }


    /**
     * Reads points and process them.
     * Complexity is O(n).
     * @return - an array of points.
     */
    private static Point[] read() {
        Scanner scanner = new Scanner(System.in);

        int count = scanner.nextInt();

        Point[] points = new Point[count * 2];

        for (int i = 0; i < count; ++i) {
            int x1 = scanner.nextInt();
            int y1 = scanner.nextInt();
            int x2 = scanner.nextInt();
            int y2 = scanner.nextInt();

            LineSegment lineSegment = new LineSegment(x1, y1, x2, y2);

            points[2 * i]     = lineSegment.left;
            points[2 * i + 1] = lineSegment.right;
        }

        return points;
    }

    /**
     * Find two line segments which intersect. If no such segments returns null.
     * Complexity is O(nlog(n)).
     * @param points - an array of points
     * @return - a pair of two line segments which intersect. If no such segments returns null.
     */
    private static Pair<LineSegment, LineSegment> findIntersect(Point[] points) {
        MergeSort.sort(points);

        // I don't need a values, just keys.
        AVLTree<LineSegment, NullType> tree = new AVLTree<>();

        for (Point point : points) {
            if (point.isLeft()) {
                LineSegment lineSegment = point.lineSegment;

                tree.insert(lineSegment, null);

                Pair<LineSegment, NullType> successor = tree.getSuccessor(lineSegment);

                if (successor != null && isIntersect(lineSegment, successor.getKey())) {
                    return new Pair<>(lineSegment, successor.getKey());
                }

                Pair<LineSegment, NullType> predecessor = tree.getPredecessor(lineSegment);

                if (predecessor != null && isIntersect(lineSegment, predecessor.getKey())) {
                    return new Pair<>(lineSegment, predecessor.getKey());
                }
            }
            else {
                LineSegment lineSegment = point.lineSegment;

                Pair<LineSegment, NullType> successor;

                try {
                    successor = tree.getSuccessor(lineSegment);
                }
                catch (Exception exc) {
                    // Nu such element. It is already removed.
                    // I guess it is because of repeating elements in the input.
                    continue;
                }

                Pair<LineSegment, NullType> predecessor = tree.getPredecessor(lineSegment);

                if (successor != null && predecessor != null && isIntersect(successor.getKey(), predecessor.getKey())) {
                    return new Pair<>(successor.getKey(), predecessor.getKey());
                }

                tree.delete(lineSegment);
            }
        }

        return null;
    }

    /**
     * Check if two line segments intersect.
     * Complexity is O(1).
     * @param first - first line segment.
     * @param second - second line segment.
     * @return - true if intersect else false.
     */
    private static boolean isIntersect(LineSegment first, LineSegment second) {
        Point p1 = first.left;
        Point p2 = first.right;
        Point p3 = second.left;
        Point p4 = second.right;

        int o1 = getOrientation(p1, p2, p3);
        int o2 = getOrientation(p1, p2, p4);
        int o3 = getOrientation(p3, p4, p1);
        int o4 = getOrientation(p3, p4, p2);

        // Fist checks that orientations o1 != o2 != 0 and o3 != o4 != 0.
        // Then checks other four cases like in the book.
        return ((o1 < 0 && 0 < o2) || (o1 > 0 && 0 > o2)) &&
               ((o3 < 0 && 0 < o4) || (o3 > 0 && 0 > o4)) ||
               (o1 == 0 && isOnSegment(p1, p2, p3)) ||
               (o2 == 0 && isOnSegment(p1, p2, p4)) ||
               (o3 == 0 && isOnSegment(p3, p4, p1)) ||
               (o4 == 0 && isOnSegment(p3, p4, p2));
    }

    /**
     * Get a orientation of the triplet.
     * Complexity is O(1).
     * @param p1 - point #1 of the triplet.
     * @param p2 - point #2 of the triplet.
     * @param p3 - point #3 of the triplet.
     * @return - Zero if points are collinear, positive number if clockwise and negative number if counterclockwise.
     */
    private static int getOrientation(Point p1, Point p2, Point p3) {
        int x1 = p3.x - p1.x;
        int y1 = p3.y - p1.y;

        int x2 = p2.x - p1.x;
        int y2 = p2.y - p1.y;

        // Cross product.
        return  (x1 * y2) - (x2 * y1);
    }

    /**
     * Check if point #3 lies on the line segment (point #1, point #2).
     * Complexity is O(1).
     * @param p1 - point #1 of the line segment.
     * @param p2 - point #2 of the line segment.
     * @param p3 - point #3 to be checked.
     * @return - true if point #3 lies on the line segment else false.
     */
    private static boolean isOnSegment(Point p1, Point p2, Point p3) {
        return (Math.min(p1.x, p2.x) <= p3.x && p3.x <= Math.max(p1.x, p2.x)) &&
               (Math.min(p1.y, p2.y) <= p3.y && p3.y <= Math.max(p1.y, p2.y));
    }
}

/**
 * An AVL binary search tree.
 * @param <K> - type of the Key items, should implements Comparable interface.
 * @param <V> - type of the Values. You can get a Value by Key.
 */
class AVLTree<K extends Comparable<K>, V> {
    /**
     * The class which is representing a single node.
     * @param <K> - type of the Key item. Should implements Comparable interface.
     * @param <V> - type of the Value item.
     */
    private static class Node<K extends Comparable<K>, V> {
        private K key;
        private V value;

        // Children.
        private Node<K, V> left;
        private Node<K, V> right;

        // Maximal size of the subtrees.
        private int height;

        public Node(K key, V value) {
            this.key   = key;
            this.value = value;

            left  = null;
            right = null;

            // Only `this` node.
            height = 1;
        }
    }

    // Top node. Can be null.
    private Node<K, V> root = null;

    /**
     * Insert a Key with Value into the tree or update if given key already exists.
     * Complexity is O(log(n))
     * @param key - key to be inserted or updated.
     * @param value - value. You can get it by this Key.
     */
    public void insert(K key, V value) {
        root = insert(root, key, value);
    }

    /**
     * Delete a Key from the tree. If no such key throws an Exception.
     * Complexity is O(log(n)).
     * @param key - key to be deleted.
     */
    public void delete(K key) throws NullPointerException, NoSuchElementException {
        if (isEmpty()) {
            throw new NullPointerException("The tree is empty.");
        }

        root = delete(root, key);
    }

    /**
     * Get a value by the key. If no such key throes an Exception.
     * Complexity is O(log(n)).
     * @param key - key, value which is stored by this key will be returned.
     * @return - a value which is stored by given key.
     */
    public V get(K key) throws NullPointerException, NoSuchElementException {
        if (isEmpty()) {
            throw new NullPointerException("The tree is empty.");
        }

        return find(root, key).value;
    }

    /**
     * Is the tree empty?
     * Complexity is O(1).
     * @return - true if tree is empty else false.
     */
    public boolean isEmpty() {
        return root == null;
    }

    /**
     * Get a successor for the given key. If no such key throws Exception. If no successor returns null.
     * Complexity is O(log(n)).
     * @param key - a key, for which a successor will be find.
     * @return a pair(Key, Value) of the successor.
     */
    public Pair<K, V> getSuccessor(K key) throws NullPointerException, NoSuchElementException {
        if (isEmpty()) {
            throw new NullPointerException("The tree is empty.");
        }

        Node <K, V> successor = getSuccessor(find(root, key));

        if (successor == null) {
            return null;
        }

        return new Pair<>(successor.key, successor.value);
    }

    /**
     * Get a predecessor for the given key. If no such key throws Exception. If no predecessor returns null.
     * Complexity is O(log(n)).
     * @param key - a key, for which a predecessor will be find.
     * @return a pair(Key, Value) of the predecessor.
     */
    public Pair<K, V> getPredecessor(K key) throws NullPointerException, NoSuchElementException {
        if (isEmpty()) {
            throw new NullPointerException("The tree is empty.");
        }

        Node <K, V> predecessor = getPredecessor(find(root, key));

        if (predecessor == null) {
            return null;
        }

        return new Pair<>(predecessor.key, predecessor.value);
    }




    /**
     * Return a height of the of the node.
     * Complexity is O(1).
     * @param node - a node to get height.
     * @return - a height. If node is null returns zero.
     */
    private int getHeight(Node<K, V> node) {
        if (node == null) {
            return 0;
        }

        return node.height;
    }

    /**
     * Compute balance factor of the node.
     * Complexity is O(1).
     * @param node - a node to compute balance factor for.
     * @return - a balance factor.
     */
    private int getBalanceFactor(Node<K, V> node) {
        return getHeight(node.left) - getHeight(node.right);
    }

    /**
     * Recompute a height for the node.
     * Complexity is O(1).
     * @param node - a node for which recompute height.
     */
    private void updateHeight(Node<K, V> node) {
        int leftHeight  = getHeight(node.left);
        int rightHeight = getHeight(node.right);

        if (leftHeight > rightHeight) {
            node.height = leftHeight + 1;
        }
        else {
            node.height = rightHeight + 1;
        }
    }


    /**
     * Insert a new key, or update value for a given key in the tree which root is `node`.
     * Complexity is O(log(n)).
     * @param node - the root of the tree.
     * @param key - a key to be inserted.
     * @param value - a value to be insert with given key.
     * @return - a new root for a given tree.
     */
    private Node<K, V> insert(Node<K, V> node, K key, V value) {
        // Create a new node.
        if (node == null) {
            return new Node<>(key, value);
        }

        // If key < node.key than go to left.
        if (node.key.compareTo(key) > 0) {
            node.left = insert(node.left, key, value);
        }
        // If key > node.key than go to right.
        else if (node.key.compareTo(key) < 0) {
            node.right = insert(node.right, key, value);
        }
        // If key == node.key than we already have this node. Just update a value.
        else {
            node.value = value;
        }

        return balance(node);
    }

    /**
     * Delete a key in the tree which root is `node`. If no such key - throws an Exception.
     * Complexity is O(log(n)).
     * @param node - the root of the tree.
     * @param key - a key to be deleted.
     * @return - a new root for a given tree.
     */
    private Node<K, V> delete(Node<K, V> node, K key) throws NoSuchElementException {
        if (node == null) {
            throw new NoSuchElementException(String.format("There is no node with key: %s", key.toString()));
        }

        // If key < node.key than go to left.
        if (node.key.compareTo(key) > 0) {
            node.left = delete(node.left, key);
        }
        // If key > node.key than go to right.
        else if (node.key.compareTo(key) < 0) {
            node.right = delete(node.right, key);
        }
        // // If key == node.key than we have found it.
        else {
            // If the node which is to be deleted has two children.
            if (node.left != null && node.right != null) {
                // Finding a successor. Since we know, that node.right != null,
                // than we can just go to left until end.
                Node<K, V> tmp = findMinimalKey(node.right);

                // Copy data from successor to current node.
                node.key   = tmp.key;
                node.value = tmp.value;

                // Remove the successor.
                node.right = delete(tmp, tmp.key);
            }
            // If only left child.
            else if (node.left != null) {
                node = node.left;
            }
            // If only right child.
            else if (node.right != null) {
                node = node.right;
            }
            // No children.
            else {
                node = null;
            }
        }

        return balance(node);
    }

    /**
     * Find a node with such key. If no such node in the tree - throws an Exception.
     * Complexity is O(log(n)).
     * @param node - the start point, usually root. From this point the search starts.
     * @param key - a key, node with this key will be find.
     * @return - a found node, which has given key.
     */
    private Node<K, V> find(Node<K, V> node, K key) throws NoSuchElementException {
        if (node == null){
            throw new NoSuchElementException(String.format("There is no node with key: %s", key.toString()));
        }

        // If key < node.key than go to left.
        if (node.key.compareTo(key) > 0) {
            return find(node.left, key);
        }
        // If key > node.key than go to right.
        else if (node.key.compareTo(key) < 0) {
            return find(node.right, key);
        }

        // If key == node.key than return it.
        return node;
    }

    /**
     * Balance a tree which root is `node`. If the node has a subtrees which
     * length different more than 1, then rotate the tree around this node.
     * Complexity is O(1).
     * @param node - the root of the tree to be balanced.
     * @return - a new root for a given tree.
     */
    private Node<K, V> balance(Node<K, V> node) {
        if (node == null) {
            return null;
        }

        updateHeight(node);

        int balanceFactor = getBalanceFactor(node);

        if (balanceFactor > 1) {
            if (getBalanceFactor(node.left) < 0) {
                node = bigRotateRight(node);
            }
            else {
                node = rotateRight(node);
            }
        }
        else if (balanceFactor < -1) {
            if (getBalanceFactor(node.right) > 0) {
                node = bigRotateLeft(node);
            }
            else {
                node = rotateLeft(node);
            }
        }

        return node;
    }


    /**
     * Rotate left a tree which root is `node` around `node`.
     * Complexity is O(1).
     * @param node - a root of the tree.
     * @return - a new root for a given tree.
     */
    private Node<K, V> rotateLeft(Node<K, V> node) {
        Node<K, V> tmp;

        tmp        = node.right;
        node.right = tmp.left;
        tmp.left   = node;

        updateHeight(node);
        updateHeight(tmp);

        return tmp;
    }

    /**
     * Rotate right a tree which root is `node` around `node`.
     * Complexity is O(1).
     * @param node - a root of the tree.
     * @return - a new root for a given tree.
     */
    private Node<K, V> rotateRight(Node<K, V> node) {
        Node<K, V> tmp;

        tmp       = node.left;
        node.left = tmp.right;
        tmp.right = node;

        updateHeight(node);
        updateHeight(tmp);

        return tmp;
    }

    /**
     * Rotate right and left a tree which root is `node` around `node`.
     * Complexity is O(1).
     * @param node - a root of the tree.
     * @return - a new root for a given tree.
     */
    private Node<K, V> bigRotateLeft(Node<K, V> node) {
        node.right = rotateRight(node.right);

        return rotateLeft(node);
    }

    /**
     * Rotate left and right a tree which root is `node` around `node`.
     * Complexity is O(1).
     * @param node - a root of the tree.
     * @return - a new root for a given tree.
     */
    private Node<K, V> bigRotateRight(Node<K, V> node) {
        node.left = rotateLeft(node.left);

        return rotateRight(node);
    }


    /**
     * Get a successor for a given node. If no successor returns null.
     * Complexity is O(log(n)).
     * @param node - a node for which a successor to be find.
     * @return a node successor or null.
     */
    private Node<K, V> getSuccessor(Node<K, V> node) {
        // If node has right subtree.
        if (node.right != null) {
            return findMinimalKey(node.right);
        }

        Node<K, V> current   = root;
        Node<K, V> successor = null;

        while (current != null && current != node) {
            if (current.key.compareTo(node.key) > 0) {
                successor = current;
                current   = current.left;
            }
            else {
                current   = current.right;
            }
        }

        return successor;
    }

    /**
     * Get a predecessor for a given node. If no predecessor returns null.
     * Complexity is O(log(n)).
     * @param node - a node for which a predecessor to be find.
     * @return a node predecessor or null.
     */
    private Node<K, V> getPredecessor(Node<K, V> node) {
        // If node has left subtree.
        if (node.left != null) {
            return findMaximalKey(node.left);
        }

        Node<K, V> current   = root;
        Node<K, V> predecessor = null;

        while (current != null && current != node) {
            if (current.key.compareTo(node.key) > 0) {
                current = current.left;
            }
            else {
                predecessor = current;
                current     = current.right;
            }
        }

        return predecessor;
    }


    /**
     * Find a minimal key for a given tree.
     * Complexity is O(log(n)).
     * @param node - a root of the tree.
     * @return - a node with minimal key.
     */
    private Node<K, V> findMinimalKey(Node<K, V> node) {
        if (node.left == null) {
            return node;
        }

        return findMinimalKey(node.left);
    }

    /**
     * Find a maximal key for a given tree.
     * Complexity is O(log(n)).
     * @param node - a root of the tree.
     * @return - a node with maximal key.
     */
    private Node<K, V> findMaximalKey(Node<K, V> node) {
        if (node.right == null) {
            return node;
        }

        return findMaximalKey(node.right);
    }
}

/**
 * Merge sorting algorithm.
 * Out-place, because I use additional memory (Create a new arrays).
 * Stable, because merge sort is stable sort algorithm.
 * Complexity is O(nlog(n)), because merge is O(n), and and there is 2 sub-problems size of n/2 on each iteration.
 * Paradigm is Divide and Conquer.
 * Usage: MergeSort.sort(array); The `array` will be sorted.
 */
class MergeSort {
    /**
     * Sort an `array` of items which are comparable.
     * Complexity is O(nlog(n)).
     * @param array - an array to be sorted. It will be modified.
     * @param <T> - type of items in the array. Should implements a Comparable interface.
     */
    public static <T extends Comparable<T>> void sort(T[] array) {
        // If size is one, then an array is already sorted.
        if (array.length == 1) {
            return;
        }

        // Find middle of the array.
        int middle = array.length / 2;

        // Divide.
        T[] left  = Arrays.copyOfRange(array, 0, middle);
        T[] right = Arrays.copyOfRange(array, middle, array.length);

        // Conquer. Sort left and right parts of the array separately.
        sort(left);
        sort(right);

        // Pointers to the current unwritten item in each array.
        int leftPtr  = 0;
        int rightPtr = 0;

        // Merge.
        for (int writeTo = 0; writeTo < array.length; ++writeTo) {
            // If we already write all left array.
            if (leftPtr == left.length) {
                // Fast copy remaining items of the right array.
                System.arraycopy(right, rightPtr, array, writeTo, array.length - writeTo); return;
            }

            // If we already write all right array.
            if (rightPtr == right.length) {
                // Fast copy remaining items of the left array.
                System.arraycopy(left, leftPtr, array, writeTo, array.length - writeTo); return;
            }

            // Copy minimal element to the array.
            if (left[leftPtr].compareTo(right[rightPtr]) > 0) {
                // If current item of the left array is bigger then copy right element.
                array[writeTo] = right[rightPtr++];
            }
            else {
                // If current item of the right array is bigger then copy left element.
                array[writeTo] = left[leftPtr++];
            }
        }
    }
}