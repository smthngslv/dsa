package dsa.labs.lab8;

import java.util.Scanner;

public class BuildingBST {
    public static void main(String[] args) {
        BST<Integer> bsd = new BST<>();

        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();

        for (int i = 0; i < N; ++i) {
            bsd.add(scanner.nextInt());
        }

        bsd.out();
    }
}

class BST<T extends Comparable> {
    private class Node<T> {
        private T value;

        private int index;

        private Node<T> left;
        private Node<T> right;

        public  Node(T value, Node<T> left, Node<T> right) {
            this.value = value;

            this.left  = left;
            this.right = right;

            // Default.
            index = -1;
        }
    }

    private Node<T> root = null;
    private int     size = 0;

    private int index;

    public void add(T value) {
        Node<T> node = root;

        while (node.left != null || node.right != null) {
            if (node.value.compareTo(value) < 0) {
                node.right = new Node<>(value, null, null); return;
            }
        }


        ++size;

        // First time.
        if (root == null) {
            root = new Node<>(value, null, null); return;
        }

        //Node<T> node = findNode(root, value);

        // If node.value > value.
        if (node.value.compareTo(value) < 0) {
            node.right = new Node<>(value, null, null); return;
        }

        // If node.value < value.
        if (node.value.compareTo(value) > 0) {
            node.left = new Node<>(value, null, null); return;
        }

        // If node.value == value. Only update.
        node.value = value; --size;
    }

    public void out() {
        System.out.println(size);

        if (root != null) {
            index = 1;
            setIndexes(root);
            printIndexes(root);
        }

        System.out.println(root.index);

    }

    private void setIndexes(Node<T> from) {
        // Indexes will store.
        from.index = index++;

        if (from.left != null) {
            setIndexes(from.left);
        }

        if (from.right != null) {
            setIndexes(from.right);
        }
    }

    private void printIndexes(Node<T> from) {
        System.out.printf("%d %d %d\n", from.value,
                from.left  == null ? -1 : from.left.index,
                from.right == null ? -1 : from.right.index);


        if (from.left != null) {
            printIndexes(from.left);
        }

        if (from.right != null) {
            printIndexes(from.right);
        }
    }

    private Node<T> findNode(Node<T> node, T value) {
        // If node.value > value.
        if (node.value.compareTo(value) < 0) {
            if (node.right == null) {
                return node;
            }

            return findNode(node.right, value);
        }

        // If node.value < value.
        if (node.value.compareTo(value) > 0) {
            if (node.left == null) {
                return node;
            }

            return findNode(node.left, value);
        }

        // If node.value == value.
        return node;
    }
}

