package dsa.labs.lab11;

import java.util.ArrayList;
import java.util.Stack;

public class CourseSchedule {
    private final byte NOT_PROCESSED = 0;
    private final byte PROCESSING    = 1;
    private final byte PROCESSED     = 2;

    private byte[] status;
    private Stack<Integer> order;
    private ArrayList<ArrayList<Integer>> adjacencyList;

    public int[] findOrder(int numCourses, int[][] prerequisites) {
        // Default values is zeros.
        status = new byte[numCourses];

        // Order will be reversed.
        order  = new Stack<>();

        // We have a sparse graph.
        adjacencyList = new ArrayList<>(numCourses);

        // To avoid nulls.
        for (int i = 0; i < numCourses; ++i) {
            adjacencyList.add(new ArrayList<>());
        }

        // Build a graph.
        for (int[] prerequisite : prerequisites) {
            adjacencyList.get(prerequisite[1]).add(prerequisite[0]);
        }

        try {
            // Solve.
            for (int node = 0; node < numCourses; ++node) {
                dfs(node);
            }
        }
        catch (IllegalStateException exception) {
            // No solutions.
            return new int[0];
        }

        int[] answer = new int[numCourses];

        // Copy to answer.
        for (int i = 0; i < numCourses; ++i) { answer[i] = order.pop(); }

        return answer;
    }

    private void dfs(int node) {
        // There is a cycle.
        if (status[node] == PROCESSING) {
            throw new IllegalStateException("There is a cycle. No solution.");
        }

        // Node was already processed.
        if (status[node] == PROCESSED) {
            return;
        }

        status[node] = PROCESSING;

        for (int neighbor : adjacencyList.get(node)) {
            dfs(neighbor);
        }

        status[node] = PROCESSED;

        order.add(node);
    }
}
