package dsa.labs.lab11;

public class KokoEatsBananas {
    public int minEatingSpeed(int[] piles, int H) {
        // Speed borders.
        int lowerBound = 1;
        int upperBound = (int)1e9;

        // Binary search.
        while (lowerBound < upperBound) {
            int middle = (lowerBound + upperBound) / 2;

            System.out.println(middle);

            if (check(middle, H, piles)) {
                upperBound = middle;
            }
            else {
                lowerBound = middle + 1;
            }
        }

        return lowerBound;
    }

    public boolean check(int speed, int hours, int[] piles) {
        for (int pile : piles) {
            hours -= (pile + (speed - 1)) / speed;

            if (hours < 0) {
                return false;
            }
        }

        return true;
    }
}
