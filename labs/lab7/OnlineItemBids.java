package dsa.labs.lab7;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class OnlineItemBids {
    public static void main(String[] args) {
        // I use kind of radix sort. Firstly, sort by current bid.
        // We have 0 <= L <= 100.
        ArrayList<Pair<Integer, Integer>>[] sorted = new ArrayList[101];

        Scanner scanner = new Scanner(System.in);

        // Number of items.
        final int N = scanner.nextInt();

        // Read.
        for (int i = 0; i < N; ++i) {
            final int L = scanner.nextInt();
            final int H = scanner.nextInt();

            if (sorted[L] == null) {
                sorted[L] = new ArrayList<>();
            }

            // Sort bu current.
            sorted[L].add(new Pair<>(i + 1, H));
        }


        for (int i = sorted.length - 1; i >= 0; --i) {
            if (sorted[i] == null) {
                continue;
            }

            // Then sort by maximum bid.
            if (sorted[i].size() > 1) {

            }

            for (int j = 0; j < sorted[i].size(); ++j) {
                System.out.print(sorted[i].get(j).getKey() + " ");
            }
        }
    }

    private static void quickSort(ArrayList<Pair<Integer, Integer>> array, int from, int to) {
        // Indexes.
        int more  = to;
        int pivot = from;


        while (true) {
            if (array.get(pivot).getValue() > array.get(pivot + 1).getValue()) {
                swap(array, pivot, ++pivot);
            }
            else {
                swap(array, pivot, more++);
            }
        }
    }

    private static void swap(ArrayList<Pair<Integer, Integer>> array, int i, int j) {
        Pair<Integer, Integer> tmp = array.get(i);

        array.set(i, array.get(j));
        array.set(j, tmp);
    }
}
